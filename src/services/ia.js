import {COHERE_API_GENERATE_URL, COHERE_API_KEY } from "../../config.js"

export async function getQuestion() {
    const data = {
      model: 'command-xlarge-nightly',
      prompt: `Generate a single "Would You Rather" question for a informal meeting in order to know us each other a little better without being too personal. Generate only the question, not the answer.`,
      max_tokens: 182,
      temperature: 0.9,
      k: 0,
      p: 0.75,
      stop_sequences: ['--'],
      return_likelihoods: 'NONE'
    }
  
    const response = await fetch(COHERE_API_GENERATE_URL, {
      method: 'POST',
      headers: {
        Authorization: `BEARER ${COHERE_API_KEY}`,
        "Content-Type": 'application/json',
        "Cohere-Version": '2022-12-06'
      },
      body: JSON.stringify(data)
    }).then(res => res.json())
  
    const { text } = response.generations[0]
    return text
  }
  