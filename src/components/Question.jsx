import { useState } from "react";
import css from "./styles"
import { getQuestion } from "../services/ia.js"

export default function Question({ question }) {

    const [ratherQuestion, setRatherQuestion] = useState(question);
    const handleClick = async () => {
        const value = await getQuestion()
        setRatherQuestion(value)
        console.log(value)
        console.log(ratherQuestion)
    }

    return (<>
        <section className="card">
            <h2>{ratherQuestion}</h2>
        </section>
        <button onClick={handleClick}>Get new question</button>
    <style jsx="true">{css}</style>
    
    </>);
}