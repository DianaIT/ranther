const css = `
button {
    background-color: rgb(137, 103, 239);
    margin: .5rem 0;
    color: white;
    font-weight: bold;
    width: 100%;
    padding: 1rem;
    border-radius: 0.5rem;
    font-size: large;
}
h2 {
    padding: 2rem 1rem;
    margin: 0 auto;
    font-size: 1.25rem;
    transition: color 0.6s cubic-bezier(0.22, 1, 0.36, 1);
}
.card {
    list-style: none;
    display: flex;
    padding: 0.15rem;
    background-color: white;
    background-image: var(--accent-gradient);
    background-size: 400%;
    border-radius: 0.5rem;
    background-position: 100%;
    transition: background-position 0.6s cubic-bezier(0.22, 1, 0.36, 1);
    box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -2px rgba(0, 0, 0, 0.1);
}
`
export default css;